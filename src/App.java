
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1('e');
        subTask2(" dcresource ");
        subTask3("the");
        subTask4();
        subTask5();
        subTask6(4);
        subTask7(2);
        subTask8();
        subTask9();
        subTask10();
    }

    public static void subTask1(char chartCount) {
        String str = "DCresource: JavaScript Exercises";
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == chartCount) {
                count++;
            }
        }
        System.out.println("Số lần xuất hiện của ký tự '" + chartCount + "' trong chuỗi là: " + count);
    }

    public static void subTask2(String string) {
        string.trim();
        System.out.println(string);
    }

    public static void subTask3(String substring) {

        String str = "The quick brown fox jumps over the lazy dog";
        String result = str.replace(substring, "");

        System.out.println("the replaced string is" + result);
    }

    public static void subTask4() {
        String str1 = "JS PHP PYTHON";
        String str2 = "PYTHON";
        String str3 = "JS";

        boolean endsWithStr2 = str1.endsWith(str2);
        boolean endsWithStr3 = str1.endsWith(str3);

        System.out.println("str1: " + str1);
        System.out.println("str2: " + str2);
        System.out.println("str3: " + str3);
        System.out.println("str1 ends with str2: " + endsWithStr2);
        System.out.println("str1 ends with str3: " + endsWithStr3);
    }

    public static void subTask5() {
        String str1 = "abcd";
        String str2 = "AbcD";
        boolean equalsIgnoreCase = str1.equalsIgnoreCase(str2);
        System.out.println("str1 equalsIgnoreCase str2: " + equalsIgnoreCase);
    }

    public static void subTask6(int n) {
        String str = "Js STRING EXERCISES";

        boolean isUppercase = Character.isUpperCase(str.charAt(n - 1));
        System.out.println("Character at position n is uppercase: " + isUppercase);
    }

    public static void subTask7(int n) {
        String str = "Js STRING EXERCISES";

        boolean isLowercase = Character.isLowerCase(str.charAt(n - 1));
        System.out.println("Character at position n is lowercase: " + isLowercase);
    }

    public static void subTask8() {
        String str1 = "js string exercises";
        String str2 = "js";

        boolean startsWithStr2 = str1.startsWith(str2);
        System.out.println("str1 starts with str2: " + startsWithStr2);
    }

    public static void subTask9() {
        String str1 = "abc";
        String str2 = "";

        boolean isEmptyStr1 = str1.isEmpty();
        boolean isEmptyStr2 = str2.isEmpty();
        System.out.println("str1 is empty: " + isEmptyStr1);
        System.out.println("str2 is empty: " + isEmptyStr2);
    }

    public static void subTask10() {
        String str1 = "AaBbc";
        String reversedStr1 = new StringBuilder(str1).reverse().toString();
        System.out.println("Reversed str1: " + reversedStr1);
    }

}
